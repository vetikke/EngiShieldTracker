
-- Keybindings ---------------------------------------------------------
BINDING_HEADER_ESTHEADER = "|cffcc41c7Engi Shield|r |cff309ec9Tracker|r"
BINDING_NAME_SWAP = "Swap Engineering Shield/Previous"
BINDING_NAME_EQUIPUNBROKEN = "Equip unbroken Engineering Shield"
BINDING_NAME_EQUIPABOVETHRESHOLD = "Equip Engineering Shield above set durability threshold"
------------------------------------------------------------------------
-- Globals --
ENGISHIELDNAME = "Force Reactive Disk"

local function EST_GetItemName(itemLink)
	local _,_,id = strfind(itemLink,"\124Hitem:(%d+)")
	return GetItemInfo(id)
end

local function EST_IsEngiShieldEquipped()
	local offhand = GetInventoryItemLink("player",17)

	if not offhand or EST_GetItemName(offhand) ~= ENGISHIELDNAME then
		return false
	end

	return true
end

local function EST_GetDurabilityTextInfo(g)
	for i=1, g:NumLines() do
		local line = getglobal("MyTooltipScanTextLeft"..i):GetText()
		local _,_,duratext, current, max = string.find(line, "(%w+) (%d+) / (%d+)")
		if duratext then
			if duratext == "Durability" then
				return i, tonumber(current), tonumber(max)
			end
		end
	end
end

local function EST_GetBagSlotItemDurability(bag, slot)
	local g=MyTooltipScan or CreateFrame("GameTooltip","MyTooltipScan",UIParent,"GameTooltipTemplate") 
	g:SetOwner(UIParent,"ANCHOR_NONE")
	g:SetBagItem(bag, slot)

	local _,current,max = EST_GetDurabilityTextInfo(g)
	local percentageLeft = (current/max)*100
	return percentageLeft
end

local function EST_GetBagPosition(itemName, ignoreBroken, lowerbound)
    for bag = 0, NUM_BAG_SLOTS do
        for slot = 1, GetContainerNumSlots(bag) do
            local link = GetContainerItemLink(bag, slot)
            if link then
                containerItemName = EST_GetItemName(link)
                if containerItemName == itemName then
                	if (not ignoreBroken) or (EST_GetBagSlotItemDurability(bag, slot) > lowerbound) then
                    	return bag, slot
                    end
                end
            end
        end
    end
end

local function EST_GetItemDurability(itemName, isEquipped)
	local g=MyTooltipScan or CreateFrame("GameTooltip","MyTooltipScan",UIParent,"GameTooltipTemplate") 
	g:SetOwner(UIParent,"ANCHOR_NONE") 
	if isEquipped then 
		g:SetInventoryItem("player",17) 
	else 
		local bag, slot = EST_GetBagPosition(itemName, true, 0)
		g:SetBagItem(bag, slot)
	end

	local index = EST_GetDurabilityTextInfo(g)

	local textline_durability = getglobal("MyTooltipScanTextLeft"..index):GetText()
	local _,_,_,current,max = string.find(textline_durability, "(%w+) (%d+) / (%d+)")
	current = tonumber(current)
	max = tonumber(max)
	return current, max
end

local function EST_EquipShield(bag, slot, setPrevious)
	if not bag then
		return
	end

	if setPrevious then
		est_previously_equipped_item = EST_GetItemName(GetInventoryItemLink("player",17))
	end
	PickupContainerItem(bag, slot)
	PickupInventoryItem(17)
end

local function EST_EquipPrevious()
	if not est_previously_equipped_item then
		return
	end

	local bag, slot = EST_GetBagPosition(est_previously_equipped_item, true, 0)
	if bag then
		PickupContainerItem(bag, slot)
		PickupInventoryItem(17)
	end 
end

function EST_EquipEngiShieldAboveDurabilityThreshold()
	if EST_IsEngiShieldEquipped() then
		local currentDura = EST_GetItemDurability(ENGISHIELDNAME, true)
		if currentDura > est_durability_threshold then
			return
		end
	end

	local bag, slot = EST_GetBagPosition(ENGISHIELDNAME, true, est_durability_threshold)
	if bag then
		EST_EquipShield(bag, slot, false)
	end
end

function EST_EquipUnbrokenEngiShield()
	local bag, slot = EST_GetBagPosition(ENGISHIELDNAME, true, 0)
	if not bag then
		return
	end

	if not EST_IsEngiShieldEquipped() then
		EST_EquipShield(bag, slot, false)
		return
	end

	if EST_GetItemDurability(ENGISHIELDNAME, true) == 0 then
		EST_EquipShield(bag, slot, false)
	end
end

function EST_EquipShieldOrPrevious()
	if EST_IsEngiShieldEquipped() then
		EST_EquipPrevious()
		return
	end

	local bag, slot = EST_GetBagPosition(ENGISHIELDNAME, true, 0)
	EST_EquipShield(bag, slot, true)
end

function EST_Message(msg)
	DEFAULT_CHAT_FRAME:AddMessage("|cffcc41c7EngiShieldTracker:|r |cff309ec9" .. msg .. "|r");
end

local function EST_UpdateTextures(durability_fraction)
	local alpha_red = (1 - durability_fraction)
	local alpha_green = (1 - alpha_red)

	shield_tex_overlay_red:SetAlpha(alpha_red)
	shield_tex_overlay_green:SetAlpha(alpha_green)
	shield_percentage_font:SetText(math.ceil(durability_fraction*100))
end

local function EST_UpdateDurability()
	shield_percentage_font:SetAlpha(0.9)
	
	local current,max = EST_GetItemDurability(ENGISHIELDNAME, true)
	if not est_durability_max then
		est_durability_max = max
		est_durability_current = current
	end

	EST_UpdateTextures(current/max)

	est_durability_current = current
end

local function EST_Command_Default()
	EST_Message("/est cmd args*")
	EST_Message("Command     	 Args         Action");
	EST_Message("-------------------------------------------------------------------")
	EST_Message("Lock               None        Lock or unlock shield frame.")
	EST_Message("Scale             0.5-2.0     Scale shield frame.")
	EST_Message("Lowerbound   0.1-99.9   Set lower durability bound.")
	EST_Message("-------------------------------------------------------------------")
	EST_Message("Check keybindings for Engi Shield Tracker functions!")
end

local function EST_Command_Scale(arg)
	local tmpScale = tonumber(arg)

	if not tmpScale or tmpScale < 0.5 or tmpScale > 2 then
		EST_Message("Scale needs to be a value between 0.5-2.0. Current: " .. string.format("%.2f", EngiShieldTrackerFrame:GetScale()))
		return
	end

	est_scale = tmpScale
	EngiShieldTrackerFrame:SetScale(est_scale)
	EngiShieldTrackerFrame:ClearAllPoints()
	EngiShieldTrackerFrame:SetPoint("CENTER", UIParent, "CENTER", 50, 50)
	EST_Message("Scale set to " .. est_scale)
end

local function EST_Command_Lock()
	if not est_locked then
		est_locked = true
		EST_Message("Frame locked.")
	else
		est_locked = false
		EST_Message("Frame unlocked.")
	end
end

local function EST_Command_SetLowerbound(lowerbound)
	local tmpThreshold = tonumber(lowerbound)

	if not tmpThreshold or tmpThreshold < 0.1 or tmpThreshold > 99.9 then
		EST_Message("Lowerbound needs to be a value between 0.1-99.9. Current: " .. est_durability_threshold)
		return
	end

	est_durability_threshold = tmpThreshold
	EST_Message("Lowerbound set to " .. est_durability_threshold .. ".")
end

function EST_Command(msg)
	local msg = string.lower(msg)
	local _,_,cmd,arg = string.find(msg,"(%S+)%s*(%S*)")

	if cmd == "lock" then
		EST_Command_Lock()
	elseif cmd == "scale" then
		EST_Command_Scale(arg)
	elseif cmd == "lowerbound" then
		EST_Command_SetLowerbound(arg)
	else
		EST_Command_Default()
	end
end

local function EST_EngiShieldNotEquipped()
	shield_percentage_font:SetAlpha(0.5)
	shield_tex_overlay_red:SetAlpha(0)
	shield_tex_overlay_green:SetAlpha(0)
end

local function EST_VARIABLES_LOADED()
	if est_scale then
		EngiShieldTrackerFrame:SetScale(est_scale)
	end

	if EST_IsEngiShieldEquipped() then
		EST_UpdateDurability()
	else
		EST_EngiShieldNotEquipped()
	end

	if not est_durability_threshold then
		est_durability_threshold = 5
	end
end

function EST_OnEvent()
	if event=="VARIABLES_LOADED" then
		EST_VARIABLES_LOADED()
		return
	end

	if not EST_IsEngiShieldEquipped() then
		EST_EngiShieldNotEquipped()
		return
	end

	EST_UpdateDurability()
end

function EST_OnLoad()
	this:RegisterEvent("UNIT_INVENTORY_CHANGED");
	this:RegisterEvent("UNIT_COMBAT");
	this:RegisterEvent("VARIABLES_LOADED");

	EST_Message("Loaded. Slashcommand: /est");
	SLASH_ENGISHIELDTRACKER1 = "/est";
	SlashCmdList["ENGISHIELDTRACKER"] = EST_Command;
end