## Interface: 11100
## Title: |cffcc41c7Engi Shield|r |cff309ec9Tracker|r
## Notes: Visually track engineering shield durability
## Author: Agateophobia
## SavedVariablesPerCharacter: est_locked, est_previously_equipped_item, est_scale, est_durability_threshold
EngiShieldTracker.xml